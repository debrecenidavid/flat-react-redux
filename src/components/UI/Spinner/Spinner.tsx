import React from 'react';
import classes from './Spinner.module.css'

type PropType = {
    height?: string
}
const Spinner: React.FC<PropType> = props => (
    <div style={{ height: props.height || 'auto', display: 'flex', alignItems: 'center', justifyContent: 'center' }}>
        <div className={classes.Spinner}>Loading...</div>
    </div>
)

export default Spinner;