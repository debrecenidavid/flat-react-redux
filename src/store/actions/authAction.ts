import * as actionTypes from './actionTypes'

//Auth action creater
export const signIn = (payload:any) =>{
    return {
        type:actionTypes.SIGN_IN,
        payload:payload
    }
}