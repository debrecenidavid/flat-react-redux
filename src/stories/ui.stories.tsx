import React from 'react';
import { storiesOf } from "@storybook/react";
import ImageViewer from '../components/UI/ImageViewer/ImageViewer';
import '../index.css';//need to import for fontawesome
import {images} from '../dummy/dummy';
import Spinner from '../components/UI/Spinner/Spinner';
import Button from '../components/UI/Buttons/Button';
import CheckboxButton from '../components/UI/Buttons/CheckButton';
import RadioButton from '../components/UI/Buttons/RadioButton';
import Input from '../components/UI/Inputs/Input';
import InputRange from '../components/UI/Inputs/InputRange';

storiesOf("UI", module)
        .add("Slider", () => <ImageViewer view='slider' width='40vw' height='45vh' mode='cover' images={images()} />)
        .add("valami", () => <Button width='50%'>Valami</Button>)
        .add("Galery", () => <ImageViewer view='gallery' width='30vw' images={images()} />)
        .add("Spinner", () => <Spinner />)
        .add("Button", () => <Button>Load more...</Button>)
        .add("CheckboxButton", () =>
                <div style={{ margin: '5rem', border: '1px solid purple', padding: '1rem', backgroundColor: '#ececec', width: '60vw', height: '80vh' }}>
                        <CheckboxButton onClick={() => console.log('garage')}>Garage</CheckboxButton>
                </div>)
        .add("RadioButton", () =>
                <div style={{ margin: '5rem', border: '1px solid purple', padding: '1rem', backgroundColor: '#ececec', width: '60vw', height: '80vh' }}>
                        <RadioButton select={(val:any)=>console.log('radio selected',val)} active='For Sale' layer={['For Sale','For Rent']}></RadioButton>
                </div>)
        .add("Input", () =>
                <div style={{ margin: '5rem', border: '1px solid purple', padding: '1rem', backgroundColor: '#ececec', width: '60vw', height: '80vh' }}>
                        <Input onChange={()=>console.log('Input')} value='asd' placeholder="dddd"></Input>
                </div>)
        .add("InputRange", () =>
                <div style={{ margin: '5rem', border: '1px solid purple', padding: '1rem', backgroundColor: '#ececec', width: '60vw', height: '80vh' }}>
                        {/* <InputRange></InputRange> */}
                </div>)