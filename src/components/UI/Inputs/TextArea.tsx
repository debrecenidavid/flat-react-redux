import React from 'react';
import classes from './TextArea.module.css'
type PropType = {
    width?: string;
    onChange: (val: any) => void;
    value: string;
    placeholder: string;
}
const TextArea: React.FC<PropType> = props => (
    <textarea value={props.value} onChange={(event) => props.onChange(event.target.value)} className={classes.Textarea} style={{ width: props.width || '48%' }} placeholder={props.placeholder} ></textarea>
);

export default TextArea;