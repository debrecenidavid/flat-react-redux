import React from 'react';
import ResultItem from './ResultItem/ResultItem';
import classes from './ResultItems.module.css';
import { Result } from '../../../types';
import { withRouter } from 'react-router';
import { useDispatch } from 'react-redux';
import { setSelectedResult } from '../../../store/actions/searchAction';

const ResultItems: React.FC<any> = props => {

    const dispatch=useDispatch()

    const handleNav=(id:any)=>{
        let selected = props.results.find((res:any)=>res.id===id);
        dispatch(setSelectedResult(selected))
        props.history.push(`/details/${id}`)
    }
    const resultContent = props.results.map((res: Result, i: any) => {
        return (
            <ResultItem onClick={()=>handleNav(res.id)} key={i} imgs={res.imgs || []} size={res.size}
                beds={res.beds} price={res.price} address={res.address} />
        )
    })
    return (
        <div className={classes.ResultItems}>
            <React.Fragment>
                <h4>{props.results.length} MATCH FOR YOU:</h4>
                <div className={classes.ResultList}>
                    {resultContent}
                </div>
            </React.Fragment>
        </div>
    )
};

export default withRouter(ResultItems);