// import axios from '../../axios-instance'
import { put, select } from 'redux-saga/effects'
import { fetchResultSucces, fetchResultStart, fetchResultFailed, fetchMoreResultSucces, setLastResult, setSelectedResult } from '../actions/searchAction';
import { Result, Store } from '../../types'
// import { getRents, createBedsIndex } from '../../dummy/dummy';
import firebase from '../../firebase'
// import { createRoomIndex } from '../../dummy/dummy';


export function* fetchSelectedResultSaga(action: any) {
    try {
        yield put(fetchResultStart('loading'))
        let doc = yield firebase.firestore().collection('rents').doc(action.id).get()
        if (doc.exists) {
            yield put(setSelectedResult({ id: action.id, ...doc.data() }))
        } else {
            console.error('soooz doc not exists');
            yield put(fetchResultFailed(true))
        }

    } catch (error) {
        yield put(fetchResultFailed(error))
    }


}
export function* fetchResultSaga(action: any) {

    try {
        yield put(fetchResultStart('loading'))
        const store: Store = yield select();
        const fetchedResults: Result[] = [];

        let rentsRef = yield firebase.firestore().collection('rents')
        // .where('bedsIndex','array-contains','10-10')

        rentsRef = createFilter(store, rentsRef)

        const res = yield rentsRef
            .orderBy('price')
            .limit(10).get()

        let last = res.docs[res.docs.length - 1];
        yield put(setLastResult(last))

        res.forEach((elem: any) => {
            fetchedResults.push({
                id: elem.id,
                ...elem.data()
            })
        });

        // const save = fetchedResults.pop();
        // const del =  yield fetchedResults.forEach((element:any) => {
        //     firebase.firestore().collection('rents').doc(element.id).delete()
        // });
        // const add = yield getRents().forEach((val: any) => {
        //      firebase.firestore().collection('rents').add(val)
        //      console.log(val)
        // })
        // console.table(fetchedResults)

        yield put(fetchResultSucces(fetchedResults))
    } catch (error) {
        yield put(fetchResultFailed(error))
    }
}
export function* fetchMoreResultSaga(action: any) {
    try {
        yield put(fetchResultStart('moreLoading'))
        const store: Store = yield select();
        const fetchedResults: Result[] = [];

        if (store.search.lastResult) {
            let rentsRef = yield firebase.firestore().collection('rents')
            rentsRef = createFilter(store, rentsRef);
            const res = yield rentsRef.orderBy('price').startAfter(store.search.lastResult).limit(10).get()
            if (res.docs.length > 0) {
                let last = res.docs[res.docs.length - 1];
                yield put(setLastResult(last))
            } else {
                yield put(setLastResult(null))
            }

            res.forEach((elem: any) => {
                fetchedResults.push({
                    id: elem.id,
                    ...elem.data()
                })
            });
        }
        yield put(fetchMoreResultSucces(fetchedResults))
    } catch (error) {
        yield put(fetchResultFailed(error))
    }
}

const createFilter = (store: Store, rentsRef: any) => {
    if (store.search.baseFilters.city !== '') {
        rentsRef = rentsRef.where('address.city', '==', `${store.search.baseFilters.city}`);
    }
    if (store.search.baseFilters.for === 'For Sale') {
        rentsRef = rentsRef.where('forSale', '==', true)
    } else if (store.search.baseFilters.for === 'For Rent') {
        rentsRef = rentsRef.where('forSale', '==', false)
    }
    if (store.search.baseFilters.type === 'House') {
        rentsRef = rentsRef.where('house', '==', true)
    } else if (store.search.baseFilters.type === 'Flat') {
        rentsRef = rentsRef.where('house', '==', false)
    }

    let price = store.search.baseFilters.price
    let beds = store.search.baseFilters.beds;
    if (price.from !== '') {
        rentsRef = rentsRef.where('price', '>=', Number(price.from));
    }
    if (price.to !== '') {
        rentsRef = rentsRef.where('price', '<=', Number(price.to));
    }

    //this beds index hack is necessary because only one inequality accepted in firebase 
    let bedsFrom = '1';
    if (beds.from !== '') {
        bedsFrom = beds.from;
    }
    bedsFrom = Number(bedsFrom) > 10 ? '10' : bedsFrom

    let bedsTo = '10'
    if (beds.to !== '') {
        bedsTo = beds.to;
    }
    bedsTo = Number(bedsTo) > 10 ? '10' : bedsTo
    rentsRef = rentsRef.where('bedsIndex', 'array-contains', `${bedsFrom}-${bedsTo}`);

    const advancedFilters: any = { ...store.search.filters }
    for (const key in advancedFilters) {
        if (advancedFilters[key].active) {
            rentsRef = rentsRef.where(`advanced.${key}`, '==', true)
        }
    }
    return rentsRef;
}