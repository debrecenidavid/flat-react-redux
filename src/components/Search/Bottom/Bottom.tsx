import React from 'react';
import classes from './Bottom.module.css';

const Bottom: React.FC<any> = () => {
    return (
        <div className={classes.Bottom}>
            <div>
                {/* <i className="fa fa-dollar" /> */}
                <i className="fas fa-wallet" />
                <span>FREE</span>
            </div>
            <div>
                <i className="fas fa-lock" />
                <span>SAFE</span>
            </div>
            <div>
                <i className="fas fa-check" />
                <span>EASY</span>
            </div>
        </div>
    )
};

export default Bottom;