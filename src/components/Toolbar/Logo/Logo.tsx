import React from 'react'
import headerLogo from '../../../assets/img/logo.png'
import classes from './Logo.module.css'
import { Link } from 'react-router-dom'

const logo = (props: any) => (
    <div style={{ height: props.height }} className={classes.Logo}>
        <Link to="/"> 
        <img src={headerLogo} alt="MyLogo" />
        </Link>
    </div>
)

export default logo;