import React from 'react';
import classes from './Toolbar.module.css';
import Logo from './Logo/Logo'
import NavigationItems from './NavigationItems/NavigationItems';

const toolbar: React.FC<any> = () => {
    return (
        <div className={classes.Toolbar}>
            <Logo height="100%" />
            <NavigationItems/>
        </div>
    )
};

export default toolbar;