import React from 'react';
// import classes from './Register.module.css'
import ImageViewer from '../../UI/ImageViewer/ImageViewer';
import { images } from '../../../dummy/dummy';

const Register: React.FC<any> = () => {
    return (
        // <div className={classes.Register}>Register coming soon!</div>
        <div style={{ height: '100vh', padding: '3rem 0.5rem' }}>
            <ImageViewer view='gallery' width='20vw' images={images()} />
        </div>
    )
};

export default Register;