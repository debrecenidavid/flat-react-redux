declare global {
    interface Window {
        __REDUX_DEVTOOLS_EXTENSION_COMPOSE__?: any
    }
}

export type Store = {
    search: SearchState,
    auth: AuthState
}
export type AuthState = {
    authenticated: boolean
}

export type SearchState = {
    results: Result[],
    selectedResult?:Result,
    loading: boolean,
    moreLoading: boolean,
    error: boolean,
    mapView: boolean,
    filters: {},
    baseFilters: BaseFilters,
    lastResult: any
}

export type BaseFilters = {
    city: string,
    for: string,
    type: string,
    view: string,
    price: {
        from: string,
        to: string
    }
    beds: {
        from: string,
        to: string
    }
}
export type Result = {
    id?: string,
    title?: string,
    description?: string,
    forSale: boolean,
    house: boolean,
    size: number,
    beds: number,
    imgs?: string[],
    price: number,
    address: Address
    // location?: {
    //     lat: string,
    //     lng: string
    // },
    location?: any,
    advanced?: {
        onlyPic: boolean,
        furnitured: boolean,
        balkon: boolean,
        garage: boolean,
        pet: boolean,
        nonsmoker: boolean,
        new: boolean,
        garden: boolean,
        child: boolean,
        ac: boolean,
    }

}

export type Address = {
    country: string,
    state: string,
    city: string,
    street: string,
    num: string,
    zip?: string
}