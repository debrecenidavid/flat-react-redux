import React from 'react';
import { Route, Switch } from 'react-router-dom';
import classes from './App.module.css'
import Layout from './hoc/Layout';
import Finder from './containers/Finder/FInder';
import Register from './components/Auth/Register/Register';
import SignIn from './components/Auth/SignIn/SignIn';
import Details from './components/Details/Details';

const App: React.FC = () => {
  return (
    <div className={classes.App}>
      <Layout>
        <Switch>
          <Route path="/signin" component={SignIn} />
          <Route path="/register" component={Register} />
          <Route path="/details/:id" exact component={Details} />
          <Route path="/" component={Finder} />
        </Switch>
      </Layout>
    </div>
  );
}

export default App;
