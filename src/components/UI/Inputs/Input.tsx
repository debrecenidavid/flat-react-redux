import React from 'react';
import classes from './Input.module.css'

type PropType = {
    width?: string;
    onChange: (val: any) => void;
    value: string;
    placeholder: string;
}
const Input: React.FC<PropType> = props => (
    <input type="text" value={props.value} onChange={(event) => props.onChange(event.target.value)} className={classes.Input} style={{ width: props.width || '48%' }} placeholder={props.placeholder} />
);

export default Input;