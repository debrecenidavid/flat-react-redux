import React, { useState, useRef, useEffect } from 'react';
import classes from './ImageViewer.module.css'
import Backdrop from '../Backdrop/Backdrop';
import Image from '../Image/Image';

type PropTypes = {
    images: string[],
    onClick?: () => void,
    width?: string,
    height?: string,
    mode?: 'cover' | 'contain',
    view?: 'slider' | 'gallery',
}
const ImageViewer: React.FC<PropTypes> = props => {
    const [selected, setSelected] = useState(0)
    const [fullSelected, setfullSelected] = useState(0)
    const [iconSize, setIconSize] = useState(3)
    const [open, setOpen] = useState(false)
    const ref = useRef<HTMLDivElement>(null)

    useEffect(() => {
        //Necessary because mediaquery check the full page width
        if (ref.current && ref.current.clientWidth <= 350) {
            setIconSize(prevState => prevState / 2)
        }
    }, [])

    const handleLeftNav = (fullScreen: boolean) => {
        if (fullScreen) {
            if (fullSelected === 0) {
                setfullSelected(props.images.length - 1)
            } else {
                setfullSelected(prevState => prevState - 1)
            }
        } else {
            if (selected === 0) {
                setSelected(props.images.length - 1)
            } else {
                setSelected(prevState => prevState - 1)
            }
        }
    }
    const handleRightNav = (fullScreen: boolean) => {
        if (fullScreen) {
            if (fullSelected + 1 === props.images.length) {
                setfullSelected(0)
            } else {
                setfullSelected(prevState => prevState + 1)
            }
        } else {
            if (selected + 1 === props.images.length) {
                setSelected(0)
            } else {
                setSelected(prevState => prevState + 1)
            }
        }
    }
    const handleImgClick = (i: any) => {
        if (props.onClick) {
            props.onClick()
        } else {
            setfullSelected(i)
            setOpen(true);
        }
    }

    const paginator: any = [];
    const fullPaginator: any = [];

    props.images.forEach((img: any, i: any) => {
        paginator.push(
            <i key={i}
                className={`fas fa-circle ${i === selected ? classes.Active : ''}`}
                style={{ fontSize: `${iconSize / 3}rem` }}
                onClick={() => setSelected(i)} />
        );
        fullPaginator.push(
            <i key={i}
                className={`fas fa-circle ${i === fullSelected ? classes.Active : ''}`}
                style={{ fontSize: '1rem' }}
                onClick={() => setfullSelected(i)} />

        );
    });

    const size = {
        width: props.width || '30vh',
        height: props.height || '30vh',
        backgroundSize: props.mode || 'cover',
    }

    const handleKeyPress = (e: any) => {
        if (e.key === 'ArrowLeft') {
            handleLeftNav(true)
        } else if (e.key === 'ArrowRight') {
            handleRightNav(true)
        } else if (e.key === 'Escape') {
            setOpen(false)
        }
    }
    let imageViewer = (
        <div ref={ref} tabIndex={-1} onKeyDown={(e) => handleKeyPress(e)} className={classes.Slider} style={size}>
            <Image onClick={() => handleImgClick(selected)} src={props.images[selected]} mode={props.mode || 'cover'} width='100%' height='100%' />
            <div className={classes.NavLeft} onClick={() => handleLeftNav(false)}>
                <i className='fas fa-angle-left' style={{ fontSize: `${iconSize}rem` }} />
            </div>
            <div className={classes.NavRight} onClick={() => handleRightNav(false)}>
                <i className='fas fa-angle-right' style={{ fontSize: `${iconSize}rem` }} />
            </div>
            <div className={classes.Paginator}>
                {paginator}
            </div>
        </div>
    )
    if (props.view === 'gallery') {
        imageViewer = (
            <div className={classes.Gallery}>
                {props.images.map((img: any, i: any) => <div key={img} onClick={() => handleImgClick(i)} style={{ backgroundImage: `url("${img}")`, backgroundSize: 'contain', ...size }} className={classes.GalleryImage} />
                )}
            </div>
        )
    }
    return (
        <React.Fragment>
            <Backdrop show={open} onClick={() => setOpen(false)} />
            {imageViewer}
            <div style={{ backgroundImage: `url("${props.images[fullSelected]}")` }} className={`${classes.ImageViewerFull} ${open ? classes.Open : ''}`}>
                <div onClick={() => handleRightNav(true)} className={classes.ImageToggle} />
                <div className={classes.NavLeft} onClick={() => handleLeftNav(true)}>
                    <i className='fas fa-angle-left' style={{ fontSize: '3rem' }} />
                </div>
                <div className={classes.NavRight} onClick={() => handleRightNav(true)}>
                    <i className='fas fa-angle-right' style={{ fontSize: '3rem' }} />
                </div>
                <div className={classes.Paginator}>
                    {fullPaginator}
                </div>
            </div>
        </React.Fragment>
        // </div>
    )
};

export default ImageViewer;