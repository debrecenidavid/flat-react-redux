import React from 'react';
import classes from './Footer.module.css';
import { Link } from 'react-router-dom';

const Footer: React.FC<any> = () => {
    return (
        <ul className={classes.Footer}>
            <li>
                <Link to="/terms"> Terms of Use </Link>
            </li>
            <li>
                <Link to="/cookies"> Cookies </Link>
            </li>
            <li>
                <Link to="/contact"> Contact</Link>
            </li>
        </ul>
    )
};

export default Footer;