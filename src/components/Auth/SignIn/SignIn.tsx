import React from 'react';
// import classes from './SignIn.module.css';
import { images } from '../../../dummy/dummy';
import ImageViewer from '../../UI/ImageViewer/ImageViewer';


const SignIn: React.FC<any> = () => {
    return (
        // <div className={classes.SignIn}>Sing in coming soon!</div>
        <div style={{ height: '100vh',display:'flex',justifyContent:'center', padding: '3rem 0.5rem' }}>
            <ImageViewer view='slider' width='80%' height='60vh' mode='cover' images={images()} />
        </div>
    )
};

export default SignIn;