import * as actionTypes from './actionTypes'
import { Result } from '../../types'

//Search action creater
export const setMapView = (mapView: boolean) => {
    return {
        type: actionTypes.SEARCH_MAP_VIEW,
        mapView: mapView
    }
}
// the fetch sagaWacher will be triggered by this action
export const fetchResults = () => {
    // const re
    return {
        type: actionTypes.FETCH_RESULT,
    }
}
export const fetchResultSucces = (result: Result[]) => {
    return {
        type: actionTypes.FETCH_RESULT_SUCCESS,
        results: result
    }
}
export const fetchMoreResult = () => {
    return {
        type: actionTypes.FETCH_MORE_RESULT,
    }
}
export const fetchMoreResultSucces = (result: Result[]) => {
    return {
        type: actionTypes.FETCH_MORE_SUCCESS,
        results: result
    }
}
export const fetchResultStart = (loader: string) => {
    return {
        type: actionTypes.FETCH_RESULT_START,
        loader: loader
    }
}
export const fetchResultFailed = (error: boolean) => {
    return {
        type: actionTypes.FETCH_RESULT_FAILED,
        error: error
    }
}
export const setMoreFilters = (filterKey: any) => {
    return {
        type: actionTypes.SET_MORE_FILTERS,
        filterKey: filterKey
    }
}
export const setBaseFilters = (filterKey: any, value: string) => {
    return {
        type: actionTypes.SET_BASE_FILTERS,
        filterKey: filterKey,
        value: value
    }
}
export const setLastResult = (lastResult:any) => {
    return {
        type: actionTypes.SET_LAST_RESULT,
        lastResult: lastResult,
    }
}
export const setSelectedResult = (selectedResult:Result) => {
    return {
        type: actionTypes.SET_SELECTED_RESULT,
        selectedResult: selectedResult,
    }
}
export const fetchSelectedResult = (resultId:any) => {
    return {
        type: actionTypes.FETCH_SELECTED_RESULT,
        id:resultId
    }
}