import React from 'react';
import Footer from '../components/Footer/Footer';
import Toolbar from '../components/Toolbar/Toolbar';

const Layout: React.FC<any> = props => {
    return (
        <React.Fragment>
            <Toolbar />
            {props.children}
            <Footer />
        </React.Fragment>
    )
};

export default Layout;