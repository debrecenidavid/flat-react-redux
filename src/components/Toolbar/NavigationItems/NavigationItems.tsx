import React from 'react';
import classes from './NavigationItems.module.css'
import NavigationItem from './NavigationItem/NavigationItem';

const navigationItems = (props: any) => {
   return( <ul className={classes.NavigationItems}>
        <NavigationItem exact link="/">HOME</NavigationItem>
        <NavigationItem link="/signin">SIGNIN</NavigationItem>
        <NavigationItem link="/register">REGISTER</NavigationItem>
   </ul>)}


export default navigationItems;