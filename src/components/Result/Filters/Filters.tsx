import React, { useEffect, useRef } from 'react';
import classes from './Filters.module.css';
import RadioButton from '../../UI/Buttons/RadioButton';
import InputRange from '../../UI/Inputs/InputRange';
import Button from '../../UI/Buttons/Button';
import CheckButton from '../../UI/Buttons/CheckButton';
import { useDispatch, useSelector } from 'react-redux';
import * as actions from '../../../store/actions'
import { Store } from '../../../types';
import { setBaseFilters } from '../../../store/actions/searchAction';

const Places = (typeof window !== 'undefined') && require('places.js');

const Filters: React.FC<any> = () => {

    const dispatch = useDispatch();
    const filters: any = useSelector((state: Store) => state.search.filters);
    const baseFilters: any = useSelector((state: Store) => state.search.baseFilters);

    const moreFilterContent = Object.keys(filters).map((key: any, i) => {
        return <CheckButton key={i} onClick={() => dispatch(actions.setMoreFilters(key))} active={filters[key].active}>{filters[key].label}</CheckButton>
    })

    const ref = useRef(null)
    const handleAutoComp = (val: any) => {
        dispatch(setBaseFilters('city', val))
    }
    useEffect(() => {
        let autocomplete = Places({
            appId: 'plT627A43MTO',
            apiKey: 'c3817d27444f033f89436e0308313568',
            container: ref.current,
            templates: {
                value: function (suggestion: any) {
                    return suggestion.name;
                }
            }
        });
        autocomplete.on('change', (e: any) => handleAutoComp(e.suggestion.name));
        return () => {
            autocomplete.removeAllListeners('change');
        };
    }, [ref])

    const handleSearch = () => {
        // window.scrollTo(0, window.innerHeight);
        dispatch(actions.fetchResults())
    }

    return (
        <div className={classes.Filters}>
            <div>
                <p style={{ marginBottom: 0, textAlign: 'left' }}>View:</p>
                <RadioButton select={(val: any) => dispatch(setBaseFilters('view', val))}
                    active={baseFilters.view} layer={['List', 'Map']}>
                </RadioButton>
            </div>
            <div>
                <p style={{ marginBottom: 0, textAlign: 'left' }}>Selected City:</p>
                <input ref={ref} type="text" placeholder="Budapest.." onKeyPress={e => { if (e.key === 'Enter') handleSearch() }} value={baseFilters.city} onChange={event => dispatch(setBaseFilters('city', event.target.value))} />
            </div>
            <RadioButton select={(val: any) => dispatch(setBaseFilters('for', val))}
                active={baseFilters.for} layer={['For Sale', 'For Rent']}>
            </RadioButton>
            <RadioButton select={(val: any) => dispatch(setBaseFilters('type', val))}
                active={baseFilters.type} layer={['House', 'Flat']}>
            </RadioButton>
            <InputRange label='Price:' value={baseFilters.price} onChange={val => dispatch(setBaseFilters('price', val))} />
            <InputRange label='Beds:' value={baseFilters.beds} onChange={val => dispatch(setBaseFilters('beds', val))} />
            {/* {loading ? <Spinner height='10vh' /> : <Button onClick={() => dispatch(actions.fetchResults())}>Search</Button>} */}
            <Button onClick={handleSearch}>Search</Button>
            <p style={{ marginBottom: 0, textAlign: 'left' }}>More filters:</p>
            <div className={classes.MoreFilters}>
                {moreFilterContent}
            </div>
        </div>
    )
};

export default Filters;