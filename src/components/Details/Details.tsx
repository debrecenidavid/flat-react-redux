import React, { useState } from 'react';
import classes from './Details.module.css'
import { useSelector, useDispatch } from 'react-redux';
import { Store } from '../../types';
import { fetchSelectedResult } from '../../store/actions/searchAction';
import Spinner from '../UI/Spinner/Spinner';
import ImageViewer from '../UI/ImageViewer/ImageViewer';
import ResultMap from '../Result/ResultMap/ResultMap';
import { Redirect } from 'react-router';
import Input from '../UI/Inputs/Input';
import Button from '../UI/Buttons/Button';
import TextArea from '../UI/Inputs/TextArea';
// import { Redirect } from '@reach/router';

const Details: React.FC<any> = props => {
    const selected: any = useSelector((store: Store) => store.search.selectedResult)
    const error: any = useSelector((store: Store) => store.search.error)
    const dispatch = useDispatch();
    const [name, setName] = useState('')
    const [email, setEmail] = useState('')
    const [message, setMessage] = useState('')
    const { id } = props.match.params
    if (!selected) {
        dispatch(fetchSelectedResult(id))
    }
    let content = <Spinner />
    if (error) {
        content = <Redirect to='/' />
    }
    return (
        <div className={classes.Details}>
            {/* {content} */}
            {!selected ? content :
                <div className={classes.DetailsContent}>
                    <ImageViewer images={selected.imgs} width='100%' height='42vh' />
                    <div className={classes.Icons}>
                        <div>
                            <i className="fas fa-home" />
                            {`${selected.size}\u33A1`}
                        </div>
                        <div>
                            <i className="fas fa-bed" />
                            {selected.beds}
                        </div>
                        <div>
                            <i className="fas fa-image" />
                            {selected.imgs.length}
                        </div>
                        <div>
                            <i className="fas fa-wallet" />
                            ${selected.price}
                        </div>
                    </div>
                    < h3 style={{ textAlign: 'left', margin: '1rem' }}>Address:</h3>
                    <div style={{ textAlign: 'left', padding: '0 2rem', fontWeight: 'bold' }}>
                        {selected.address.num}.
                        {selected.address.street} street,
                        {selected.address.state},
                        {selected.address.country}
                    </div>
                    <h3 style={{ textAlign: 'left', margin: '1rem' }}>Details:</h3>
                    <div className={classes.DetailsGrid}>
                        <div className={classes.Advanced}>
                            <span>Furnitured:</span>
                            <span>{selected.advanced.furnitured ? <i className="fas fa-check" /> : <i className="fas fa-ban" />}
                            </span>
                        </div>
                        <div className={classes.Advanced}>
                            <span>AC:</span>
                            <span>{selected.advanced.ac ? <i className="fas fa-check" /> : <i className="fas fa-ban" />}
                            </span>
                        </div>
                        <div className={classes.Advanced}>
                            <span>Balkon:</span>
                            <span>{selected.advanced.balkon ? <i className="fas fa-check" /> : <i className="fas fa-ban" />}
                            </span>
                        </div>
                        <div className={classes.Advanced}>
                            <span>Child friendly:</span>
                            <span>{selected.advanced.child ? <i className="fas fa-check" /> : <i className="fas fa-ban" />}
                            </span>
                        </div>
                        <div className={classes.Advanced}>
                            <span>Garage:</span>
                            <span>{selected.advanced.garage ? <i className="fas fa-check" /> : <i className="fas fa-ban" />}
                            </span>
                        </div>
                        <div className={classes.Advanced}>
                            <span>Garden:</span>
                            <span>{selected.advanced.garden ? <i className="fas fa-check" /> : <i className="fas fa-ban" />}
                            </span>
                        </div>
                        <div className={classes.Advanced}>
                            <span>Non smoker:</span>
                            <span>{selected.advanced.nonsmoker ? <i className="fas fa-check" /> : <i className="fas fa-ban" />}
                            </span>
                        </div>
                        <div className={classes.Advanced}>
                            <span>Pet:</span>
                            <span>{selected.advanced.pet ? <i className="fas fa-check" /> : <i className="fas fa-ban" />}
                            </span>
                        </div>
                    </div>
                    <h3 style={{ textAlign: 'left', margin: '1rem' }}>Description:</h3>
                    <p style={{ background: 'rgba(0,0,0,0.05)', marginTop: '0' }}>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit,
                        sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                        Quis lectus nulla at volutpat. Duis tristique sollicitudin nibh sit amet commodo.
                        Sit amet est placerat in egestas. Sed vulputate odio ut enim blandit volutpat.
                        Commodo odio aenean sed adipiscing diam donec adipiscing tristique risus.
                        Lobortis feugiat vivamus at augue eget arcu. Elit pellentesque habitant morbi tristique senectus et netus.
                        In metus vulputate eu scelerisque felis imperdiet proin fermentum leo.
                        Facilisis leo vel fringilla est ullamcorper eget nulla facilisi.
                        Faucibus interdum posuere lorem ipsum dolor sit. Aenean sed adipiscing diam donec adipiscing tristique risus.
                        Enim lobortis scelerisque fermentum dui. Urna id volutpat lacus laoreet non.
                        Et malesuada fames ac turpis egestas maecenas pharetra convallis. Dictumst quisque sagittis purus sit amet volutpat consequat.
                        Vel eros donec ac odio tempor.
                    </p>
                    {/* <div className={classes.MapContainer} style={{ height: '50vh', width: '100%', marginBottom: '3rem' }}>
                        <ResultMap results={[selected]} width='100%' height='100%' />
                    </div> */}

                    <div className={classes.Contact}>
                        <div className={classes.ContactForm}>
                            <h4 style={{marginLeft:'3px'}}>Send a message!</h4>
                            <p style={{margin:'4px'}}>Name:</p>
                            <Input onChange={(val)=>setName(val)} width='100%' placeholder='Name' value={name}></Input>
                            <p style={{margin:'4px'}}>Email:</p>
                            <Input onChange={(val)=>setEmail(val)} value={email} width='100%' placeholder='Email'></Input>
                            <p style={{margin:'4px'}}>Message:</p>
                            {/* <textarea style={{width:'100%'}}/> */}
                            <TextArea onChange={(val)=>setMessage(val)} value={message} width='100%' placeholder='Message'/>
                            <Button>Send</Button>
                        </div>
                        <div className={classes.ContactDetails}>
                            <ResultMap results={[selected]} width='100%' height='50%' />
                            <div style={{ textAlign: 'left', padding: '1rem', fontWeight: 'bold' }}>
                                {selected.address.num}.
                        {selected.address.street} street,
                        {selected.address.state},
                        {selected.address.country}
                            </div>
                            Contact:
                            <div style={{ textAlign: "left", padding: '0.5rem 1rem' }}>
                                <i className="fas fa-user" style={{ marginRight: '6px' }} />
                                John Doe
                            </div>
                            <div style={{ textAlign: "left", padding: '0.5rem 1rem' }}>
                                <i className="fas fa-phone" style={{ marginRight: '6px' }} />
                                06123-312-456
                            </div>
                            <div style={{ textAlign: "left", padding: '0.5rem 1rem' }}>
                                <i className="fas fa-at" style={{ marginRight: '6px' }} />
                                <a href="mailto:name@email.com">test@test.com</a>
                            </div>
                        </div>
                    </div>
                </div>
            }
        </div>
    )
};

export default Details;