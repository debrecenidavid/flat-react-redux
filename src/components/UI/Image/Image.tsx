import React from 'react';
import classes from './Image.module.css';

type PropType={
    src:any,
    mode?:string,
    height?:string,
    width?:string,
    style?:React.CSSProperties,
    onClick?:()=>void
}
const Image: React.FC<PropType> = (props: any) => {
    let { mode, src, height, width, style } = props;

    let defaults = {
        height: height || 100,
        width: width || 100,
    };

    let important = {
        backgroundImage: `url("${src}")`,
        backgroundSize: mode,
    };

    return <div onClick={props.onClick} className={classes.Image} {...props} style={{ ...defaults, ...style, ...important }} />
};

export default Image;