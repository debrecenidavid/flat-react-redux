import React from 'react';
import classes from './ResultItem.module.css';
import ImageViewer from '../../../UI/ImageViewer/ImageViewer';
import { Address } from '../../../../types';

type PropType={
    onClick?:()=>void
    imgs:string[],
    size:number,
    beds:number,
    price:number,
    address:Address,
}
const ResultItem: React.FC<PropType> = props => {
    return (
        <div className={classes.ResultItem}>
            <ImageViewer view='slider' width='100%' height='9rem' images={props.imgs} />
            <div onClick={props.onClick} className={classes.ResultItemContent}>
                <div className={classes.Icons}>
                    <div>
                        <i className="fas fa-home" />
                        {`${props.size}\u33A1`}
                    </div>
                    <div>
                        <i className="fas fa-bed" />
                        {props.beds}
                    </div>
                    <div>
                        <i className="fas fa-image" />
                        {props.imgs.length}
                    </div>
                </div>
                <h4>${props.price}/mth</h4>
                <h5>{props.address.street} street {props.address.num}.</h5>
                <h5>{props.address.city}, {props.address.country}</h5>
            </div>
        </div>
    )
};

export default ResultItem;