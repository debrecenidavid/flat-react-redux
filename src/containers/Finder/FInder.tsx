import React from 'react';
import Search from '../../components/Search/Search';
import Result from '../../components/Result/Result';
import classes from './Finder.module.css';
import Filters from '../../components/Result/Filters/Filters';

const Finder: React.FC<any> = () => {
    return (
        <div className={classes.Finder}>
            <Search />
            <div className={classes.ResultFilters}>
                <Result />
                <Filters />
            </div>
        </div>
    )
};

export default Finder;