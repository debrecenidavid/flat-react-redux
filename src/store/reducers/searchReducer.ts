import * as actionTypes from '../actions/actionTypes';
import { SearchState } from '../../types';

const initialState: SearchState = {
    results: [],
    loading: false,
    moreLoading: false,
    error: false,
    mapView: false,
    lastResult: null,
    selectedResult: undefined,
    baseFilters: {
        city: '',
        for: 'For Sale',
        type: 'House',
        view: 'List',
        price: {
            from: '',
            to: ''
        },
        beds: {
            from: '',
            to: ''
        },
    },
    filters: {
        onlyPic: {
            active: false,
            label: 'Only with picture'
        },
        furnitured: {
            active: false,
            label: 'Furnitured'
        },
        balkon: {
            active: false,
            label: 'Balkon'
        },
        garage: {
            active: false,
            label: 'Garage'
        },
        pet: {
            active: false,
            label: 'Pet friendly'
        },
        nonsmoker: {
            active: false,
            label: 'Non smoker'
        },
        new: {
            active: false,
            label: 'New'
        },
        garden: {
            active: false,
            label: 'Has garder'
        },
        child: {
            active: false,
            label: 'Child friendly'
        },
        ac: {
            active: false,
            label: 'Air condition'
        },
    }
}
const reducer = (state: SearchState = initialState, action: any) => {
    switch (action.type) {
        case actionTypes.SEARCH_MAP_VIEW:
            return {
                ...state,
                mapView: action.mapView
            }
        case actionTypes.FETCH_RESULT_START:
            return {
                ...state,
                [action.loader]: true
            }
        case actionTypes.FETCH_RESULT_SUCCESS:
            return {
                ...state,
                results: [...action.results],
                loading: false
            }
        case actionTypes.FETCH_MORE_SUCCESS:
            return {
                ...state,
                results: [...state.results, ...action.results],
                moreLoading: false
            }
        case actionTypes.FETCH_RESULT_FAILED:
            console.log(action.error);
            return {
                ...state,
                error: action.error,
                loading: false,
                moreLoading: false
            }
        case actionTypes.SET_BASE_FILTERS:
            return {
                ...state,
                baseFilters: {
                    ...state.baseFilters,
                    [action.filterKey]: action.value
                }
            }
        case actionTypes.SET_LAST_RESULT:
            return {
                ...state,
                lastResult: action.lastResult
            }
        case actionTypes.SET_SELECTED_RESULT:
            return {
                ...state,
                selectedResult: action.selectedResult,
                loading: false,
                moreLoading: false
            }
        case actionTypes.SET_MORE_FILTERS:
            const key: string = action.filterKey
            const filters: any = {
                ...state.filters
            }
            const newFilter: any = {
                ...filters[key],
                active: !filters[key].active
            }

            return {
                ...state,
                filters: {
                    ...state.filters,
                    [action.filterKey]: newFilter
                }
            }
        default:
            return state
    }
}

export default reducer;