import React from 'react';
import classes from './Search.module.css';
import SearchBox from './Searchbox/Searchbox';
import Bottom from './Bottom/Bottom';

const Search: React.FC<any> = () => {
    return (
        <div className={classes.Search} style={{ height: window.innerHeight }}>
            <SearchBox />
            <Bottom />
        </div>
    )
};

export default Search;