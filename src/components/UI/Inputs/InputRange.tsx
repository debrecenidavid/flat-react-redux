import React from 'react';
import classes from './InputRange.module.css'
import Input from './Input';

type PropType = {
    value: { from: string, to: string },
    onChange: (newVal: any) => void,
    label?: string
}
const InputRange: React.FC<PropType> = props => {
    return (<div className={classes.InputRange}>
        <p>{props.label}</p>
        <Input onChange={newVal => props.onChange({ ...props.value, from: newVal })} value={props.value.from} placeholder="min."></Input>
        <Input onChange={newVal => props.onChange({ ...props.value, to: newVal })} value={props.value.to} placeholder="max."></Input>
    </div>
    )
}

export default InputRange;