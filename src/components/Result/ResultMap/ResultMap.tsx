import React, { useState, useEffect } from 'react';
import classes from './ResultMap.module.css'
import L from 'leaflet';
import { Result } from '../../../types';

const ResultMap: React.FC<{ results: Result[], width?: string, height?: string }> = props => {
    const [map, setMap] = useState();
    const [markerGroup, setMarkerGroup] = useState();
    useEffect(() => {
        // init map
        let center: any = props.results.length > 0 ?
            [props.results[0].location.latitude, props.results[0].location.longitude] : [47.5284688, 21.590596];
        let m = map ? map : L.map('map', {
            center: center,
            zoom: 13,
            minZoom: 10,
            scrollWheelZoom: false
        });

        L.tileLayer('https://{s}.tile.osm.org/{z}/{x}/{y}.png', {
        }).addTo(m)

        const newMarkerGroup = L.featureGroup();
        props.results.forEach((result: Result) => {
            let popUp = L.popup({
                className: 'pUp'
            }).setContent(`<div>
                <h1>Hello PopUp</h1>
            </div>`)
            L.marker([result.location.latitude, result.location.longitude]).addTo(newMarkerGroup).bindPopup(popUp);
        })
        // remove the old markers before add the new one
        markerGroup && markerGroup.removeFrom(map);
        newMarkerGroup.addTo(m)
        setMarkerGroup(newMarkerGroup)
        setMap(m)
    }, [props.results, map])

    return <div className={classes.ResultMap} style={{ width: props.width || '95%', height: props.height || '80vh' }} id="map"></div>
}

export default ResultMap;