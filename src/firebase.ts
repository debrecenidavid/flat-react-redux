import firebase from 'firebase/app'
import 'firebase/firestore'

const firebaseConfig = {
    apiKey: "AIzaSyB6gJCb21Dg1gzzuS7qSZ0aLxh60BcSqvc",
    authDomain: "flat-react-redux.firebaseapp.com",
    databaseURL: "https://flat-react-redux.firebaseio.com",
    projectId: "flat-react-redux",
    storageBucket: "flat-react-redux.appspot.com",
    messagingSenderId: "472831425951",
    appId: "1:472831425951:web:adfce2d6beed0e975ad1aa",
    measurementId: "G-QJD6P0FNWW"
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);

export default firebase;