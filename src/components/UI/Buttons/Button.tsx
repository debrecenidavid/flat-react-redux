import React from 'react';
import classes from './Button.module.css'

type PropType = {
  disabled?: boolean;
  width?: string;
  onClick?: () => void;
}
const Button: React.FC<PropType> = props => {
  return <button disabled={props.disabled} style={{ width: props.width || '100%' }} className={classes.Button}
    onClick={props.onClick}>{props.children}
  </button>
};

export default Button;