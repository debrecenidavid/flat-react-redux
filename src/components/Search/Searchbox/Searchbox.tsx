import React, { useRef, useEffect } from 'react';
import classes from './Searchbox.module.css';
import { Store } from '../../../types';
import * as actions from '../../../store/actions';
import { useDispatch, useSelector } from 'react-redux';
import { setBaseFilters } from '../../../store/actions/searchAction';

const Places = (typeof window !== 'undefined') && require('places.js');

const SearchBox: React.FC<any> = () => {
    const dispatch = useDispatch();
    const view = useSelector((state: Store) => state.search.baseFilters.view);
    const city = useSelector((state: Store) => state.search.baseFilters.city);
    const ref = useRef(null)

    const handleSearch = () => {
        window.scrollTo(0, window.innerHeight);
        dispatch(actions.fetchResults())
    }

    useEffect(() => {
        let autocomplete = Places({
            appId: 'plT627A43MTO',
            apiKey: 'c3817d27444f033f89436e0308313568',
            container: ref.current,
            templates: {
                value: function (suggestion: any) {
                    return suggestion.name;
                }
            }
        });
        autocomplete.on('change', (e: any) => handleAutoComp(e.suggestion.name));
        return () => {
            autocomplete.removeAllListeners('change');
        };
    }, [ref])

    const handleAutoComp = (val: any) => {
        dispatch(setBaseFilters('city', val))
    }

    return (
        <div className={classes.Searchbox}>
            <div className={classes.Choose}>
                <div onClick={() => dispatch(setBaseFilters('view', 'List'))} className={view === 'List' ? classes.Active : ''}>LOOKING FOR REAL ESTATE</div>
                <div onClick={() => dispatch(setBaseFilters('view', 'Map'))} className={view === 'Map' ? classes.Active : ''}>SHOW ON MAP</div>
            </div>
            <div className={classes.SearchbarContainer}>
                <span>WHERE ARE YOU LOOKING FOR?</span>
                <div className={classes.Searchbar}>
                    <input ref={ref} type="text" placeholder="Budapest.." value={city} onKeyPress={e => { if (e.key === 'Enter') handleSearch() }} onChange={event => dispatch(setBaseFilters('city', event.target.value))} />
                    <button onClick={handleSearch}>
                        <i className="fa fa-search"></i>
                    </button>
                </div>
            </div>
        </div>
    )
};

// const mapStateToProps = (state: Store) => {
//     return {
//         mapView: state.search.mapView
//     }
// }
// const mapDispatchToProps = (dispatch: Dispatch) => {
//     return {
//         onMapViewSet: (val: boolean) => dispatch(setMapView(val))
//     }
// }

// export default connect(mapStateToProps, mapDispatchToProps)(SearchBox);
export default SearchBox;