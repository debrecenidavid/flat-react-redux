import React from 'react';
import classes from './CheckButton.module.css'

type PropType = {
    active?: boolean;
    width?: string;
    onClick: () => void;
}
const CheckButton: React.FC<PropType> = props => {
    return (
        <button onClick={props.onClick} style={{ width: props.width || '48%' }}
            className={`${classes.CheckButton} ${props.active ? classes.Active : ''}`}>
            {props.children}
        </button>
    )
};

export default CheckButton;