import React from 'react';
// import classes from './RadioButton.module.css'
import CheckButton from './CheckButton';

type PropType={
    layer:any[],
    active:string,
    select:(val:string)=>void
}
const RadioButton: React.FC<PropType> = props => {
    return (
        <React.Fragment>
            <CheckButton onClick={()=>props.select(props.layer[0])} active={props.layer[0] === props.active}>{props.layer[0]}</CheckButton>
            <CheckButton onClick={()=>props.select(props.layer[1])} active={props.layer[1] === props.active}>{props.layer[1]}</CheckButton>
        </React.Fragment>
    )
};

export default RadioButton;