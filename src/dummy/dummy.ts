import { Result } from '../types'
import firebase from '../firebase'
const fireB1 = 'https://cdn.pixabay.com/photo/2016/06/24/10/47/architecture-1477041__340.jpg'
const fireB2 = 'https://cdn.pixabay.com/photo/2019/03/08/20/14/kitchen-living-room-4043091_960_720.jpg'
const fireB3 = 'https://indaily.com.au/wp-content/uploads/2019/04/Screen-Shot-2019-04-12-at-8.04.06-am-850x455.png'
const fireB4 = 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTeHAfMj9OiQPGE08Qo_RY7dk5jInIXMtbclI9UuioO5doJuCGw'
const fireB5 = 'https://media.istockphoto.com/photos/home-and-healthy-front-yard-during-late-spring-season-picture-id957895328?k=6&m=957895328&s=612x612&w=0&h=jE3tvHz1gReCYq_8w5WtgjOGxd06S2E0vXHJpZRrfLM='

export const images = () => {
    let unshuffled = [fireB1, fireB2, fireB3, fireB4, fireB5]
    let shuffled = unshuffled
        .map((a) => ({ sort: Math.random(), value: a }))
        .sort((a, b) => a.sort - b.sort)
        .map((a) => a.value)
    return shuffled;
};

const getTrueOrFalse = () => {
    return Math.random() > 0.5
}

//this beds index hack is necessary because only one inequality accepted in firebase 
export const createBedsIndex = (beds: number) => {
    const indexArray = []
    beds = beds > 10 ? 10 : beds
    if (indexArray.length === 0) {
        for (let from = 1; from <= 10; from++) {
            for (let to = 1; to <= 10; to++) {
                if (beds >= from && beds <= to) {
                    indexArray.push(`${from}-${to}`);
                } else if (beds > 10) {
                    indexArray.push(`${from}-${to}`);
                }
            }
        }
    }
    return indexArray;
}

export const getRents = (): Result[] => {
    const res = []
    for (let index = 0; index < 10; index++) {
        const lat = (Math.random() * (47.5022 - 47.5544) + 47.554);
        const lng = (Math.random() * (21.5950 - 21.6735) + 21.5950);
        let beds = Math.floor(1 + Math.random() * 12);
        let rent = {
            // id: Math.floor(1 + Math.random() * 10000).toString(),
            title: 'Awesome House',
            description: 'Please buy this house, i need money for more udemy course',
            // location: {
            //     lat: (Math.random() * (47.5022 - 47.5544) + 47.554).toFixed(4),
            //     lng: (Math.random() * (21.5950 - 21.6735) + 21.5950).toFixed(4)
            // },
            location: new firebase.firestore.GeoPoint(lat, lng),
            imgs: images(),
            size: Math.floor(1 + Math.random() * 100),
            price: Math.floor(500 + Math.random() * 500),
            beds: beds,
            bedsIndex: createBedsIndex(beds),
            house: getTrueOrFalse(),
            forSale: getTrueOrFalse(),
            created: firebase.firestore.FieldValue.serverTimestamp(),
            address: {
                country: 'HU',
                state: 'Hajdú-Bihar',
                city: 'Debrecen',
                street: streets[Math.floor(Math.random() * streets.length)],
                num: Math.floor(1 + Math.random() * 150).toString(),
                zip: Math.floor(1000 + Math.random() * 3000).toString(),
            },
            advanced: {
                onlyPic: getTrueOrFalse(),
                furnitured: getTrueOrFalse(),
                balkon: getTrueOrFalse(),
                garage: getTrueOrFalse(),
                pet: getTrueOrFalse(),
                nonsmoker: getTrueOrFalse(),
                new: getTrueOrFalse(),
                garden: getTrueOrFalse(),
                child: getTrueOrFalse(),
                ac: getTrueOrFalse(),
            }
        }
        res.push(rent)
    }
    return res;
}

const streets = [
    'Aczél Géza',
    'Ady Endre',
    'Akadémia',
    'Andaházi',
    'Babér',
    'Babits Mihály',
    'Balassi Bálint',
    'Bán Imre',
    'Bán Imre',
    'Bárczi Géza',
    'Bartha Boldizsár',
    'Bay Zoltán',
    'Békéssy Béla',
    'Bencsik Sándor',
    'Benczúr Gyula',
    'Bendegúz',
    'Bessenyei',
    'Bognár Rezső',
    'Bólyai',
    'Borbíró',
    'Böszörményi',
    'Branyiszkó',
    'Civis',
    'Csanak',
    'Doberdó',
    'Dóczy József',
    'Egyetem',
    'Egyetem',
    'Fejedelem',
    'Füredi',
    'Füredi',
    'Gárdonyi Géza',
    'Görgey',
    'Gulyás Pál',
    'Gyimes',
    'Gyöngyösi',
    'Halász István',
    'Hangyás',
    'Hatvani István',
    'Hollós',
    'Illyés Gyula',
    'Jenő',
    'Jerikó',
    'József Attila',
    'Kaffka Margit',
    'Kálmán Béla',
    'Karinthy Frigyes',
    'Károli Gáspár',
    'Károlyi Mihály',
    'Kartács',
    'Kéki Lajos',
    'Kér',
    'Kertváros',
    'Kiss Sámuel',
    'Komlóssy',
    'Kosztolányi Dezső',
    'Kürtgyarmat',
    'Lehel',
    'Lisznyai',
    'Lóverseny',
    'Martonfalvi',
    'Menyhárt József',
    'Mikszáth Kálmán',
    'Móricz Zsigmond',
    'Nagy Lajos Király',
    'Nagyerdei',
    'Németh László',
    'Nyék',
    'Nyulas',
    'Oláh Gábor',
    'Ormós Lajos',
    'Pallagi',
    'Pápai József',
    'Patai István',
    'Péchy',
    'Pilinszky János',
    'Poroszlay',
    'Sántha Kálmán',
    'Sestakert',
    'Soó Rezső',
    'Szalay Sándor',
    'Szarka János',
    'Szikes',
    'Takács István',
    'Tankó Béla',
    'Tarján',
    'Tessedik Sámuel',
    'Thomas Mann',
    'Thuróczi Gyula',
    'Úrrétje',
    'Varsány',
    'Verseny',
    'Vezér',
    'Vigadó',
]