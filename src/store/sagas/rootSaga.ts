import { takeLatest, takeEvery } from 'redux-saga/effects'

import * as actionTypes from '../actions/actionTypes'
import { fetchResultSaga, fetchMoreResultSaga, fetchSelectedResultSaga } from './searchSaga'

export function* wathchResult() {
    yield takeLatest(actionTypes.FETCH_RESULT, fetchResultSaga)
    yield takeLatest(actionTypes.FETCH_MORE_RESULT, fetchMoreResultSaga)
    yield takeEvery(actionTypes.FETCH_SELECTED_RESULT, fetchSelectedResultSaga)
}