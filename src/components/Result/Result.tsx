import React from 'react';
import classes from './Result.module.css';
import ResultItems from './ResultItems/ResultItems';
import Button from '../UI/Buttons/Button';
import { useDispatch, useSelector } from 'react-redux';
import { Store } from '../../types';
import Spinner from '../UI/Spinner/Spinner';
import { fetchMoreResult } from '../../store/actions';
import ResultMap from './ResultMap/ResultMap';


const Result: React.FC<any> = () => {
    const dispatch = useDispatch()
    const moreLoading = useSelector((state: Store) => state.search.moreLoading)
    const lastResult = useSelector((state: Store) => state.search.lastResult)
    const loading = useSelector((state: Store) => state.search.loading)
    const view = useSelector((state: Store) => state.search.baseFilters.view)
    const results = useSelector((state: Store) => state.search.results)

    return (
        <div className={classes.Result}>
            {
                // loading ? <Spinner height='20vh' />
                // :
                (view === 'List' ? <ResultItems results={results} /> : <ResultMap results={results} />)
            }
            <div style={{ height: '20vh',width:'100%', display: 'flex', alignItems: 'center', justifyContent: 'center' }}>
                {moreLoading || loading ? <Spinner /> :
                    <Button disabled={!lastResult} width='70%' onClick={() => dispatch(fetchMoreResult())}>Load more...</Button>}
            </div>
        </div>
    )
};

export default Result;